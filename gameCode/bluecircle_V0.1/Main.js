// You can use either `new PIXI.WebGLRenderer`, `new PIXI.CanvasRenderer`, or `PIXI.autoDetectRenderer`
// which will try to choose the best renderer for the environment you are in.
var rendererOptions = {
    antialiasing: false,
    transparent: false,
    resolution: window.devicePixelRatio,
    autoResize: true,
    backgroundColor: 0x003EB1,
}

// Declare a global variable for our sprite so that the animate function can access it.
var circles = [];
var jelly;
var background;
// You need to create a root container that will hold the scene you want to draw.
var stage; // = new PIXI.Container();
var main;

function Main() {
    main = this;
    var interactive = true;
    stage = new PIXI.Container();
    renderer = PIXI.autoDetectRenderer(375, 667, rendererOptions); //PIXI.autoDetectRenderer(375, 667, rendererOptions);
    //document.getElementById("canvas-holder").addChild(renderer.view);
    document.body.appendChild(renderer.view);
    jelly = new Jelly(main);
    jelly.animate();
    trackPad = new InteractiveTrackPad(main, stage);

    // stage.addChild(jelly);
    //var width = document.getElementById("game-canvas").width;

    //background

    //far = new Far();
    //stage.addChild(far);

    //midground
    //mid = new Mid();
    //stage.addChild(mid);

    requestAnimationFrame(update);
}



// The renderer will create a canvas element for you that you can then insert into the DOM.
//document.body.appendChild(renderer.view);




function createCircle(i, loader, resources) {

    // This creates a texture from a 'bunny.png' image.
    var circle = new PIXI.Sprite(resources.circle.texture);
    circle.interactive = true;
    circle.buttonMode = true;
    circle.on('mousedown', onFingerDown)
    circle.on('mouseup', onFingerUp)
    circle.on('touchstart', onFingerDown)
    circle.on('touchend', onFingerUp)

    circles.push(circle);
    // Setup the position and scale of the bunny

    circle.position.x = Math.round((Math.random() * 215) + 80);
    circle.position.y = 0;

    circle.anchor.x = 0.5;
    circle.anchor.y = 0.5;

    // Add the bunny to the scene we are building.
    stage.addChild(circle);
}



function createBackground(resources) {

}

function onFingerDown() {
    if (this.scale.x < 6.4) {
        this.scale.x += 1;
        this.scale.y += 1;
        //window.setTimeout(function() {
        //circle.onFingerDown();
        //}, 33); 
    }
}

function onFingerUp() {
    this.isdown = true;
    this.scale.x = 0.5;
    this.scale.y = 0.5;
    this.tint = 0xFFFFFF;
}

//do nothing state = -2
//display splash screen = -1
//main menu = 0
//gameplay = 1
//pause and menu = 2
//death  3
var state = 1; // change this to start at 0

//do nothing state = 0
//load things state = 1
var gameplayState = 1;

//update loop    
function update() {
    switch (state) {
        case -2:
            //do nothing
            break;
        case -1:
            //display some splash screen
            break;
        case 0:
            //display menu
            break;
        case 1:
            //render gameplay stuffs
            switch (gameplayState) {
                case 0:


                    // load the texture we need
                    PIXI.loader.add('circle', './images/blueCircle@2x.png').load(function (loader, resources) {
                        createCircle(0, loader, resources);
                        window.setTimeout(function () {
                            createCircle(1, loader, resources);
                        }, 2600);
                        window.setTimeout(function () {
                            createCircle(2, loader, resources);
                        }, 4600);

                        window.setTimeout(function () {
                            createCircle(3, loader, resources);
                        }, 7200);

                        window.setTimeout(function () {
                            createCircle(4, loader, resources);
                        }, 9800);
                        // kick off the animation loop (defined below)
                        animateBubbles();
                    });



                    //what state should we enter when we are finished loading gameplay stuff?
                    gameplayState = 1;
                    break;
                case 1:
                    //  console.log("loop");
                    animateBubbles();
                    jelly.animate2();
                    //jelly.call(animate());
                    break;
            }

            break;
        case 2:
            //pause time and display menu
            break;
        case 3:
            //player has died, display menu for that
            break;
    }

    renderer.render(stage);
    requestAnimationFrame(update);
}

function animateBubbles() {
    for (i = 0; i < circles.length; ++i) {
        if (circles[i].position.y < -80) {
            circles[i].position.y = 667;
            circles[i].position.x = Math.round((Math.random() * 215) + 80);
            circles[i].scale.x = 1.0;
            circles[i].scale.y = 1.0;
        }
        circles[i].position.y += 1;
    }
}
