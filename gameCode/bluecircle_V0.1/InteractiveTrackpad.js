function InteractiveTrackPad(main, stage) {
    this.stage = stage;
    mainRef = main;
    background = PIXI.Texture.fromImage('./images/placeholderbg.jpg');
    var bg = new PIXI.Sprite(background);
    stage.addChild(bg);
    setupBGInteractivity(bg);

    bg.position.y = 550;


    bg
    // set the mousedown and touchstart callback...
        .on('mouseover', onMouseMove);

}

function startAccumulatingDiffs(start) {
    startV2 = new PIXI.Point();
    startV2.x = 0;
    startV2.y = 0;
}

function applyAccumulatedToMovement(data) {

    var mouse = data.getLocalPosition(bg);
    if (main) {
        if (main.jelly) {
            print("Send animate");
            main.jelly.animate(mouse.position.x, mouse.position.y);
        } else console.log("jelly undefined");
    } else
        console.log("undefined stuff");
}

function applyAccumulatedToMovement(data) {


}

function stopAccumulating() {}

var startV2;
var movementV2;
var mainRef;
var stage;
/*
function setupBGInteractivity() {

    var _target = new PIXI.Container();
    //_target.setInteractive(true);
    _target.interactive = true;
    stage.addChild(_target);
    //stage._interactiveEventsAdded = true;
    //stage.setInteractive(true);

    movementV2 = new PIXI.Point(0, 0);

    stage.mousedown = stage.touchstart = onMouseDown;
    stage.mousemove = stage.touchmove = onMouseMove;
    stage.mouseup = stage.touchend = onMouseUp;
}
*/

function onMouseDown(data) {
    // console.log("mouse down");
}

function onMouseMove(data) {
    console.log("mouse move" + data.data.originalEvent.movementX);

    //var newPosition = this.data.getLocalPosition(this.parent);



    //if (data) {
    // var mouse = data.getLocalPosition(stage);

    var x = data.data.originalEvent.movementX;
    var y = data.data.originalEvent.movementY;

    main.jelly.animate(x, y);
    //} else console.log("data missing...");



}

function onMouseUp() {}

function setupBGInteractivity(bg) {
    movementV2 = new PIXI.Point(0, 0);

    bg.interactive = true;



    // set the mousedown and touchstart callback..
    bg.mousedown = bg.touchstart = function (data) {
        //  console.log("mouse down");
        this.isdown = true;
        //var mouse = data.getLocalPosition(bg);
        //startAccumulatingDiffs(mouse.position)
        //startAccumulatingDiffs(data.Point);

    }

    // set the mouseup and touchend callback..
    bg.mouseup = bg.touchend = function (data) {
        this.isdown = false;
        // console.log("mouse up");
        /*
        if (this.isOver) {
            this.setTexture(textureButtonOver);
        } else {
            this.setTexture(textureButton);
        }
        
        */
        // stopAccumulating();
    }


    bg.mouseenter = function (data) {
        this.isOver = true;
        // console.log("mouse enter");

        //startAccumulatingDiffs(data.Point);
    }

    // set the mouseover callback..
    bg.mouseover = function (data) {

        this.isOver = true;
        // console.log("mouse over");
        //               if (this.isdown) return

        //                this.setTexture(textureButtonOver)

        applyAccumulatedToMovement(data);
    }

    // set the mouseout callback..
    bg.mouseout = function (data) {

        this.isOver = false;
        //    console.log("mouse out");
        // if (this.isdown) return
        //    this.setTexture(textureButton)

        stopAccumulating();
    }

    bg.click = function (data) {
        // click!
        console.log("CLICK!");
        //	alert("CLICK!")
        startAccumulatingDiffs(data.Point);
    }

    bg.tap = function (data) {
        // click!
        console.log("TAP!!");
        //this.alpha = 0.5;

        startAccumulatingDiffs(data.Point);
    }

}


//InteractiveBG.prototype = Object.create(PIXI.extras.TilingSprite.prototype);

InteractiveTrackPad.prototype.update = function () {
    // this.tilePosition.y -= 0.128;
};
